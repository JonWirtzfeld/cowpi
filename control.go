package control 

import (
	"time"
	"github.com/stianeikeland/go-rpio"
)

type (
	//MooverStatus holds current state of movement
	//Forward, Backward, Left, Right, Stop
	MooverStatus string

	//Moover holds all pins for controlling Moover,
	//speed and status indicators
	Moover struct {

		LHSDir rpio.Pin
		RHSDir rpio.Pin
		
		LHSSpeed rpio.Pin
		RHSSpeed rpio.Pin

		Status MooverStatus
		Speed uint32

	}
)

const(
	//Forward indicates moover is moving forward
	Forward MooverStatus = "Forward"
	//Backward indicates moover is moving backward
	Backward MooverStatus = "Backward"
	//ForwardLeft indicates moover is turning left
	ForwardLeft MooverStatus = "ForwardLeft"
	//ForwardRight indicates moover is turning Right
	ForwardRight MooverStatus = "ForwardRight"
	//BackwardLeft indicates moover is turning left
	BackwardLeft MooverStatus = "BackwardLeft"
	//BackwardRight indicates moover is turning Right
	BackwardRight MooverStatus = "BackwardRight"
	//Stop indicates moover is NOT moving
	Stop MooverStatus = "Stop"
	//turnSpeed
	turnSpeed       uint32 = 24
	//time consts
	tenthSecond     float64 = time.Second/10
	eightiethSecond float64 = time.Second/80
	cycleLen        uint32  = 32 
)

//Forward checks to see if Moover is already moving forward,
//if so, it stops the Moover (toggle). If not, it stops the 
//Moover temporarely and then sets the pins for Forward movement.
func (m *Moover) Forward () {

	if(m.Status == Forward){
		m.Stop()
	} else {
		m.Stop()
		time.Sleep(tenthSecond)
		m.LHSDir.High()
		m.RHSDir.High()
		for i := m.Speed; i < m.Speed; i++ {
			m.LHSSpeed.DutyCycle(i, cycleLen)
			m.RHSSpeed.DutyCycle(i, cycleLen)
			time.Sleep(eightiethSecond)
		}
		m.LHSSpeed.DutyCycle(m.Speed, cycleLen)
		m.RHSSpeed.DutyCycle(m.Speed, cycleLen)

		m.Status = Forward
	}

}

//Backward checks to see if Moover is already moving backward,
//if so, it stops the Moover (toggle). If not, it stops the 
//Moover temporarely and then sets the pins for backward movement.
func (m *Moover) Backward () {

	if(m.Status == Backward){
		m.Stop()
	} else {
		m.Stop()
		time.Sleep(tenthSecond)
		m.LHSDir.Low()
		m.RHSDir.Low()
		for i := m.Speed; i < m.Speed; i++ {
			m.LHSSpeed.DutyCycle(i, cycleLen)
			m.RHSSpeed.DutyCycle(i, cycleLen)
			time.Sleep(eightiethSecond)
		}
		m.LHSSpeed.DutyCycle(m.Speed, cycleLen)
		m.RHSSpeed.DutyCycle(m.Speed, cycleLen)

		m.Status = Backward
	}

}

//ForwardLeft checks to see if Moover is already turning left,
//if so, it stops the Moover (toggle). If not, it stops the 
//Moover temporarely and then sets the pins for turning left.
func (m *Moover) ForwardLeft () {

	if(m.Status == ForwardLeft){
		m.Stop()
	} else {
		m.Stop()
		time.Sleep(tenthSecond)
		m.LHSDir.High()
		m.RHSDir.High()
		m.LHSSpeed.DutyCycle(turnSpeed, cycleLen)
		m.RHSSpeed.DutyCycle(m.Speed, cycleLen)
		m.Status = ForwardLeft
	}

}

//ForwardRight checks to see if Moover is already turning right,
//if so, it stops the Moover (toggle). If not, it stops the 
//Moover temporarely and then sets the pins for turning right.
func (m *Moover) ForwardRight () {

	if(m.Status == ForwardRight){
		m.Stop()
	} else {
		m.Stop()
		time.Sleep(tenthSecond)
		m.LHSDir.High()
		m.RHSDir.High()
		m.LHSSpeed.DutyCycle(m.Speed, cycleLen)
		m.RHSSpeed.DutyCycle(turnSpeed, cycleLen)
		m.Status = ForwardRight
	}

}

//BackwardLeft checks to see if Moover is already turning left,
//if so, it stops the Moover (toggle). If not, it stops the 
//Moover temporarely and then sets the pins for turning left.
func (m *Moover) BackwardLeft () {

	if(m.Status == BackwardLeft){
		m.Stop()
	} else {
		m.Stop()
		time.Sleep(tenthSecond)
		m.LHSDir.Low()
		m.RHSDir.Low()
		m.LHSSpeed.DutyCycle(turnSpeed, cycleLen)
		m.RHSSpeed.DutyCycle(m.Speed, cycleLen)
		m.Status = BackwardLeft
	}

}

//BackwardRight checks to see if Moover is already turning right,
//if so, it stops the Moover (toggle). If not, it stops the 
//Moover temporarely and then sets the pins for turning right.
func (m *Moover) BackwardRight () {

	if(m.Status == BackwardRight){
		m.Stop()
	} else {
		m.Stop()
		time.Sleep(tenthSecond)
		m.LHSDir.Low()
		m.RHSDir.Low()
		m.LHSSpeed.DutyCycle(m.Speed, cycleLen)
		m.RHSSpeed.DutyCycle(turnSpeed, cycleLen)
		m.Status = BackwardRight
	}

}

//Stop stops to Moover. No checks. Perhaps the most important
//function in control package
func (m *Moover) Stop () {

	if(m.Status != Stop){
		for i := m.Speed; i != 0; i-- {
			m.LHSSpeed.DutyCycle(i, cycleLen)
			m.RHSSpeed.DutyCycle(i, cycleLen)
			time.Sleep(eightiethSecond)
		}
		m.Status = Stop
	} else {
		m.LHSSpeed.DutyCycle(0, cycleLen)
		m.RHSSpeed.DutyCycle(0, cycleLen)
		m.Status = Stop
	}
}
