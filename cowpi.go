package main

import (
	"fmt"
	"html/template"
	"net/http"
	"encoding/json"
	"control"
	"github.com/stianeikeland/go-rpio"
	"os"
)

const (
	//IMPORTANT
	//For pinout go to pinout.xyz
	//use the MCU pin values
	//Example GPIO 13 (PWM1) is what
	// lhsSpeedPhysical pin is conn to
	lhsDirPhysicalPin = 23
	rhsDirPhysicalPin = 24

	lhsSpeedPhysicalPin = 12
	rhsSpeedPhysicalPin = 13
	//time consts
	cycleLen        uint32  = 32
	pinFreq         float64 = 640000

)

type mooverWrapped struct {
	wmoover *control.Moover
}
type Click struct {
	Value string
}
var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templates/*.gohtml"))
}

func main() {
	err := rpio.Open()
	if err != nil {
		os.Exit(1)
	}
	defer rpio.Close()

	lhsDirpin := rpio.Pin(lhsDirPhysicalPin)
	rhsDirpin := rpio.Pin(rhsDirPhysicalPin)
	
	lhsDirpin.Output()
	rhsDirpin.Output()

	lhsSpeedpin := rpio.Pin(lhsSpeedPhysicalPin)
	lhsSpeedpin.Mode(rpio.Pwm)
	lhsSpeedpin.Freq(pinFreq)
	lhsSpeedpin.DutyCycle(0,cycleLen)

	rhsSpeedpin := rpio.Pin(rhsSpeedPhysicalPin)
	rhsSpeedpin.Mode(rpio.Pwm)
	rhsSpeedpin.Freq(pinFreq)
	rhsSpeedpin.DutyCycle(0,cycleLen)

	moover := &control.Moover{ 
					LHSDir: lhsDirpin,
					RHSDir: rhsDirpin,

					LHSSpeed: lhsSpeedpin,
					RHSSpeed: rhsSpeedpin,
			 
					Status: control.Stop,
					Speed: 24}

	wm := mooverWrapped{wmoover: moover}

	http.HandleFunc("/", wm.mooverHandler)
	http.Handle("/serve/", http.StripPrefix("/serve", http.FileServer(http.Dir("assets"))))
	http.ListenAndServe(":8000", nil)
} 

func (m *mooverWrapped) mooverHandler(w http.ResponseWriter, r *http.Request) {
	
	if r.Method == "GET" {
		fmt.Println("Index Page requested - IP:" + r.RemoteAddr)
		tpl.ExecuteTemplate(w, "control.gohtml", nil)
	} else if r.Method == "POST" {
		var c Click
		err := json.NewDecoder(r.Body).Decode(&c)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		if(c.Value == "Up"){
			m.wmoover.Forward()
		} else if (c.Value == "ForwardLeft"){
			m.wmoover.ForwardLeft()
		} else if (c.Value == "ForwardRight"){
			m.wmoover.ForwardRight()
		} else if (c.Value == "BackwardLeft"){
			m.wmoover.BackwardLeft()
		} else if (c.Value == "BackwardRight"){
			m.wmoover.BackwardRight()
		} else if (c.Value == "Down"){
			m.wmoover.Backward()
		} else {
			m.wmoover.Stop()
		}
		}
	}
