<h1>CowPi</h1>
<p1>Software for controlling a Moover dairy fence</p1><br><br>
<img src='https://siasky.net/_AhHfX1EKglz_sAXwf-E0q_-S2T5jb4PW7CaymzC-pwAWQ' width = 30%>
<br><br><b>What is CowPi?</b><p> CowPi is a small program written to test the control system and drive-train hardware on The Moover. It is written in Golang and should run on a Raspberry Pi. It does two things: 1.) Hosts a webserver for displaying the 'Control Panel' to the users device and 2.) manages GPIO pin control signals for the motor controllers. It is only meant for proof-of-concept, many things need to be improved before legitimate use. 

Getting Started
--------------------
Not sure who is going to be using the Moover after our group, so I am including this guide here with everything a person could need to start using/modifying CowPi. 


<h3>First things first</h3>
You are going to need to configure the pi to connect to your wifi. The only way we knew how to do this was by taking the pi out of the Moover, connecting it to a monitor and configuring the wifi using the GUI. Its hdmi, the only hdmi monitors we found in egh were in the makers lab. I suggest configuring it to the mobile hotspot created by your phone, that way it can be controlled off campus without problems. 


<h3>Using CowPi</h3>
<body>Right now, CowPi runs immediately when the Pi boots. If you know the IP address of the Pi, then using CowPi is easy. Just put the IP address into the search bar of your phone internet browser, followed by the port number 8000 (example: `196.117.1.58:8000`). If everything is connected correctly, the `Control Panel` page should load, and you should be able to control the Moover. If you need the Pi IP address and the Pi is connected to the internet via your phones mobile hotspot, then your phone should list it under some `Connected Devices` listing. If not, you'll have to use the `ifconfig` command on the Pi terminal. <br> <br>

The current Control Panel has some pretty basic controls. <b>A note of caution:</b> the current version of CowPi does not know if there are connection issues between the client (phone) and server (pi). If connection is lost, the Moover will continue moving in whatever direction it was last told to. The Wifi antenna has made this less of an issue, but there is a kill switch on the side of the Moover if it needs to be stopped. <b>A bigger note of caution:</b> Flipping the kill switch does not restart the Pi, so if you dont restart the pi manually, then flipping the kill switch back off will cause the Moover to start moving again as it was when it was killed. If you flip the kill switch, always remember to unplug and replug the Pi. </body>

<h3>Modifying CowPi</h3>
<body>The current version of CowPi is PoC used to verify the hardware works. If you are looking to upload something better, you may run into an issue regarding the fact that CowPi runs on boot. Here is how I learned how to shut it off (there may be a better way): 

1. SSH into the Pi (if you do not have the GUI up): `ssh pi@<pi ipaddress>` (password is cowpi)
2. Find cowpi PID numbers: `ps aux | grep cowpi` (PID are three digit number next to program name, used to identify running programs.)
3. Kill cowpi and sudo cowpi programs: `sudo kill -9 <PID1> <PID2>` 

After these steps are taken, CowPi should no longer be running. Files can be replaced or deleted via `scp` without issue. 

If you do not want CowPi to run on boot, you need to edit the `rc.local` file. 
</body>
